#include "main.h"
#include "mem_internals.h"

static bool test_free() {
  size_t sz[N];
  sz[0] = 1024;

  for (size_t i = 1; i < N; ++i)
    sz[i] = sz[i - 1] * 2;

  for (size_t i = 0; i < N; ++i) {
    fprintf(stderr, "Test %zu: %zu", i, sz[i]);

    void *heap = heap_init(sz[i]);

    if (!heap) {
      fprintf(stderr, " failed: heap was not initialized properly\n");
      return false;
    }

    void *allocated_data = _malloc(LOCAL); 
    
    if (!allocated_data) {
      fprintf(stderr, " failed: data was not allocated properly\n");
      return false;
    }

    debug_heap(stderr, heap);

    _free(allocated_data);

    debug_heap(stderr, heap);

    heap_term();

    fprintf(stderr, " OK\n");
  }

  return true;
}
static bool test_malloc() {
  size_t sz[N];
  sz[0] = 1024;

  for (size_t i = 1; i < N; ++i)
    sz[i] = sz[i - 1] * 2;

  for (size_t i = 0; i < N; ++i) {
    fprintf(stderr, "Test %zu: %zu", i, sz[i]);

    void *heap = heap_init(sz[i]);

    if (!heap) {
      fprintf(stderr, " failed: heap was not initialized properly\n");
      return false;
    }

    void *allocated_region = _malloc(REGION_MIN_SIZE);
    void *allocated_local = _malloc(LOCAL);

    if (!allocated_region || !allocated_local) {
      fprintf(stderr, " failed: data was not allocated properly\n");
      return false;
    }

    debug_heap(stderr, heap);

    _free(allocated_region);
    if (allocated_region) {
      fprintf(stderr, " failed: region was not deallocated properly\n");
      return false;
    }

    _free(allocated_local);
    if (allocated_local) {
      fprintf(stderr, " failed: local data was not deallocated properly\n");
      return false;
    }

    debug_heap(stderr, heap);

    heap_term();

    fprintf(stderr, " OK\n");
  }

  return true;
}

static bool test_small_queries() {
  for (size_t i = 0; i < N; ++i) {
    fprintf(stderr, "Test %zu", i);
    void *h = heap_init(1024);

    if (!h) {
      fprintf(stderr, " failed: heap was not initialized properly\n");
      return false;
    }

    debug_heap(stderr, h);
    void *k;
    for (size_t j = 0; j < N/M; ++j) {
      k = _malloc(LOCAL);
    }

    if (!k) {
      fprintf(stderr, " failed: data was not allocated properly\n");
      return false;
    }

    debug_heap(stderr, h);
    _free(k);
    debug_heap(stderr, h);
    heap_term();
  }
  return true;
}

int main() {
  return !test_free() || !test_small_queries() || !test_malloc();
}
